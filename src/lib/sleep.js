/*global Promise,setTimeout*/
import logger from './logger';

const log = logger.init('sleep.js');

/**
 * Returns a Promise that resolves after a given amount of milliseconds.
 *
 * Warning: Due to implementation details, the Promise might be resolved several milliseconds _after_ the timeout.
 * @access public
 * @augments setTimeout
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Wait for 2 seconds</caption>
 * import {
 * 	assert,
 * } from '@megatherium/test';
 * import sleep from '@megatherium/sleep';
 *
 * (async() => {
 * 	const t0 = Date.now();
 * 	await sleep(2000);
 * 	assert.greaterThanOrEqual(Date.now() - t0, 2000);
 * 	assert.rejects(sleep(), {
 * 		name: 'ReferenceError',
 * 		message: '"timeoutInMilliseconds" is not defined',
 * 	});
 * })();
 * @example <caption>Wait 3000 times for 100ms</caption>
 * // test-timeout:360000
 * import {
 * 	assert,
 * } from '@megatherium/test';
 * import sleep from '@megatherium/sleep';
 *
 * (async() => {
 * 	for (let i = 0; i < 3000; ++i) {
 * 		const t0 = Date.now();
 * 		await sleep(100);
 * 		assert.greaterThanOrEqual(Date.now() - t0, 100);
 * 	}
 * })();
 * @license Megatherium Standard License 1.0
 * @module sleep
 * @param {int} timeoutInMilliseconds The amount of milliseconds to wait before resolving the timeout.
 * @returns {Promise} Resolves after the given amount of milliseconds.
 * @throws {ReferenceError} Throws a ReferenceError with `'"timeoutInMilliseconds" is not defined'` if `timeoutInMilliseconds` was not given.
 */
const sleep = async(timeoutInMilliseconds) => {
	if (typeof timeoutInMilliseconds === 'undefined') {
		throw new ReferenceError('"timeoutInMilliseconds" is not defined');
	}
	const t0 = Date.now();
	await new Promise(resolve => {
		const checkTime = () => {
			const duration = Date.now() - t0,
				remainingDuration = timeoutInMilliseconds - duration;
			
			if (remainingDuration > 0) {
				const timeout = remainingDuration - Math.ceil(remainingDuration / 100);
				setTimeout(checkTime, timeout > 0 ? timeout : 1);
			} else {
				resolve();
			}
		};

		setTimeout(checkTime, Date.now() - t0 + timeoutInMilliseconds - Math.ceil(timeoutInMilliseconds / 100));
	});
};

export default sleep;
